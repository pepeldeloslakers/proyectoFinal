package com.salesianostriana.proyecto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectofinalcursoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectofinalcursoApplication.class, args);
	}
}
